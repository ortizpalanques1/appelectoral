﻿# AppElectoral del Municipio Getafe en 2015 y 2016

Este proyecto es para el segundo módulo del curso de certificación profesional.
Su objetivo es poder comparar resultados electorales entre sí y con datos socio económicos.

## Objetivos específicos
1. Presentar los resultados de los diferentes partidos por mesa o local electoral o barrio en un municipio determinado.
2. Comparar entre mesas, centros o barrios en cada elección (sincronía).
3. Comparar los resultados en elecciones diferentes (diacronía).
4. Informar sobre los datos socio económicos en el lugar de votación de cada partido. Los datos existentes hasta ahora son edad e ingreso.
5. Relacionar para cada partido sus datos electorales con los datos electorales del barrio.
6. Los datos se presentan en forma de tablas.

## Tablas a crear
### 1. Tabla de Elección 
La clase es *Elección* y los objetos son las *resultados por barrio*. Hay una tabla para cada elección.  
1. Sus columnas son:
+ Id, 
+ barrio, 
+ resultado por partidos desde P1 hasta Pn, 
+ número total de votantes, 
+ número real de votantes, 
+ votos blancos, y
+ votos nulos.
2. Sus métodos son: 
+ totalizar el municipio, 
+ totalizar la participación.
3. Sus vistas: comparar partidos en cada elección

### 2. Tabla de barrios  
Recoge los datos socioeconómicos del barrio. La clase es *Municipio Getafe* y los objetos son los *barrios*.
1. Sus columnas son: 
+ Población total, 
+ Cantidad del grupo de edad 1, hasta cantidad del grupo de edad n, 
+ ingresos promedio del barrio.
2. No tiene métodos própios.

### 3. Métodos de la base de datos
1. Comparar un partido a través de diversas elecciones.
2. Comparar entre barrios en una misma elección.
3. Crear para cada barrio con rresultados electorales y datos socioeconómicos.
4. Relacionar para cada partido en cada barrio sus resultados electorales y socio económicos.

### 4. Características de la aplicación
1. Base de datos en MySQL.
2. Un server en PHP.
3. Un server en Express.
4. Visor en html con jQuery.

### 5. Avances hasta ahora
1. Se ha creado el viewer en html.
2. Hoy se creará un server para la primera función.


